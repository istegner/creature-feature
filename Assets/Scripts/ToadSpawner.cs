﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToadSpawner : MonoBehaviour
{
    public bool debug = true;
    public PathManager pathManager;
    public GameSettings gameSettings;

    public float scale = 0.05f;
    public float offsetX = 50f;
    public float offsetY = 50f;

    public GameObject toadPrefab;
    public Transform toadParent;

    public int toads;

    private void OnEnable()
    {
        EventManager.OnNodesGenerated += SpawnToads;
    }

    private void OnDisable()
    {
        EventManager.OnNodesGenerated -= SpawnToads;
    }

    void SpawnToads()
    {
        if (debug) Debug.Log("Spawning toads");
        toads = 0;
        if (pathManager == null)
            pathManager = FindObjectOfType<PathManager>();
        for (int i = 0; i < pathManager.pathDataNodes.Count; i++)
        {
            var node = pathManager.pathDataNodes[i];
            if (node.walkable)
            {
                var chance = CalculateChance(node.worldPos.x, node.worldPos.y);
                if (chance < gameSettings.toadSpawnChance)
                {
                    if (gameSettings.toadDensity > Random.Range(0f, 1f))
                    {
                        SpawnToad(node.worldPos);
                    }
                }
            }
        }
        if (debug) Debug.Log("Done spawning frogs. Spawned " + toads + " frogs");
    }

    public void SpawnToad(Vector3 pos)
    {
        var newToad = Instantiate(toadPrefab, pos, toadPrefab.transform.rotation, toadParent);
        newToad.name = "Toad" + toads;
        toads++;
    }

    float CalculateChance(float x, float y)
    {
        float xCoord = (x + offsetX) / (pathManager.width * scale);
        float yCoord = (y + offsetY) / (pathManager.height * scale);
        return Mathf.PerlinNoise(xCoord, yCoord);
    }
}
