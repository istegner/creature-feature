﻿using System.IO;
using UnityEngine;

public class GameLoader : MonoBehaviour
{
    //Where to save and load the data from
    private string agentDataPath;

    //The prefabs of the agents to instantiate
    public GameObject frogPrefab;
    public GameObject toadPrefab;
    public GameObject carPrefab;

    //The parents for the agents
    public Transform frogParent;
    public Transform toadParent;
    public Transform carParent;

    private void OnEnable()
    {
        agentDataPath = Application.streamingAssetsPath + "/AgentData.txt";
    }

    /// <summary>
    /// Read the AgentData file line-by-line and instantiate a new GO using the JsonUtility
    /// </summary>
    public void LoadGame()
    {
        //If there is save data
        if (File.Exists(agentDataPath))
        {
            //Read the save data line by line
            foreach (string line in File.ReadLines(agentDataPath))
            {
                NewAgent newAgent = JsonUtility.FromJson<NewAgent>(line);
                GameObject agentGO;
                if (newAgent.isFrog || newAgent.isToad)
                {
                    if (newAgent.isFrog)
                    {
                        agentGO = Instantiate(frogPrefab, newAgent.position, frogPrefab.transform.rotation, frogParent);
                        agentGO.GetComponent<BehaviourController>().isFrog = true;
                    }
                    else
                    {
                        agentGO = Instantiate(toadPrefab, newAgent.position, toadPrefab.transform.rotation, toadParent);
                        agentGO.GetComponent<BehaviourController>().isToad = true;
                    }
                    agentGO.GetComponent<FrogController>().foodToMate = newAgent.foodToMate;
                    agentGO.GetComponent<Vitals>().foodLevel = newAgent.foodLevel;
                    agentGO.name = newAgent.name;
                }
                else if (newAgent.isCar)
                {
                    agentGO = Instantiate(carPrefab, newAgent.position, carPrefab.transform.rotation, carParent);
                    agentGO.GetComponent<BehaviourController>().isCar = true;
                    agentGO.name = newAgent.name;
                }
            }
            Time.timeScale = 1.0f;
            Debug.Log("Loaded game data");
        }
        else
        {
            Debug.LogError("No save data found to load");
        }
    }

    /// <summary>
    /// Get a list of all GO's with behaviour controllers on them
    /// Cycle through the list and create a NewAgent based on its data
    /// Use JsonUtility to turn it into json and write it to the AgentData file
    /// </summary>
    public void SaveGame()
    {
        //Get a list of every agent
        var agents = FindObjectsOfType<BehaviourController>();

        if (File.Exists(agentDataPath))
        {
            File.Delete(agentDataPath);
            File.Create(agentDataPath).Dispose();
        }
        else
            File.Create(agentDataPath).Dispose();

        string toWrite = "";
        for (int i = 0; i < agents.Length; i++)
        {
            NewAgent newAgent = new NewAgent();
            newAgent.position = agents[i].transform.position;
            newAgent.name = agents[i].gameObject.name;
            newAgent.isFrog = agents[i].isFrog;
            newAgent.isToad = agents[i].isToad;
            newAgent.isCar = agents[i].isCar;
            if(newAgent.isFrog || newAgent.isToad)
            {
                newAgent.foodLevel = agents[i].GetComponent<Vitals>().foodLevel;
                newAgent.foodToMate = agents[i].GetComponent<FrogController>().foodToMate;
            }
            else
            {
                newAgent.foodLevel = 0;
                newAgent.foodToMate = 0;
            }
            toWrite += JsonUtility.ToJson(newAgent) + '\n';
        }
        File.WriteAllText(agentDataPath, toWrite);
        Debug.Log("Saved data to file " + agentDataPath);
    }
}

/// <summary>
/// All the information required to save/load agents
/// </summary>
[System.Serializable]
public class NewAgent
{
    public Vector3 position;
    public string name;
    public bool isFrog;
    public bool isToad;
    public bool isCar;
    public int foodLevel;
    public int foodToMate;
}
