﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour
{
    public bool debug = true;
    public PathManager pathManager;
    public GameSettings gameSettings;
    public GameObject carPrefab;
    public Transform carParent;
    GameObject ground;
    int cars;

    private void OnEnable()
    {
        EventManager.OnNodesGenerated += SpawnCars;
    }

    private void OnDisable()
    {
        EventManager.OnNodesGenerated -= SpawnCars;
    }

    void SpawnCars()
    {
        if (debug) Debug.Log("Spawning Cars");
        ground = GameObject.FindGameObjectWithTag("Ground");
        var carsToSpawn = gameSettings.numRCCars;
        for(int i = 0; i < carsToSpawn; i++)
        {
            var pos = RandomPoint(ground.GetComponent<MeshCollider>().bounds);
            SpawnCar(pos);
        }
        if (debug) Debug.Log("Done spawning cars. Spawned " + cars + " cars");
    }

    void SpawnCar(Vector3 pos)
    {
        var newCar = Instantiate(carPrefab, pos, carPrefab.transform.rotation, carParent);
        newCar.name = "Car" + cars;
        cars++;
    }

    Vector3 RandomPoint(Bounds bounds)
    {
        var randPoint = new Vector3(Random.Range(bounds.min.x, bounds.max.x), 0.5f, Random.Range(bounds.min.z, bounds.max.z));
        if (pathManager.WorldToNode(randPoint).walkable)
            return randPoint;
        return RandomPoint(bounds);
    }
}
