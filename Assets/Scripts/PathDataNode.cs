﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PathDataNode
{
    public int index;
    public Vector3 worldPos;
    public bool walkable;
    public GameObject sphere;

    public PathDataNode(Vector3 _worldPos, bool _walkable)
    {
        worldPos = _worldPos;
        walkable = _walkable;
    }

    public PathDataNode(int _index, Vector3 _worldPos, bool _walkable)
    {
        index = _index;
        worldPos = _worldPos;
        walkable = _walkable;
    }

    public PathDataNode(int _index, Vector3 _worldPos, bool _walkable, GameObject _sphere)
    {
        index = _index;
        worldPos = _worldPos;
        walkable = _walkable;
        sphere = _sphere;
    }
}
