﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PathFindingNode : System.IEquatable<PathFindingNode>
{
    public float g;
    public float h;
    public PathFindingNode parentNode;
    public PathDataNode pathdataNode;

    public float f
    {
        get
        {
            return g + h;
        }
    }

    public PathFindingNode(PathDataNode _pathDataNode)
    {
        pathdataNode = _pathDataNode;
    }

    public override bool Equals(object other)
    {
        PathFindingNode temp = (PathFindingNode)other;
        if (temp == null)
            return false;
        return temp.pathdataNode.index == pathdataNode.index;
    }

    public bool Equals(PathFindingNode other)
    {
        if(other == null)
            return false;
        return other.pathdataNode.index == pathdataNode.index;
    }

    public override int GetHashCode()
    {
        return pathdataNode.index;
    }

    public static bool operator ==(PathFindingNode node1, PathFindingNode node2)
    {
        if (ReferenceEquals(node1, null) || ReferenceEquals(node2, null))
            return false;
        return node1.Equals(node2);
    }

    public static bool operator !=(PathFindingNode node1, PathFindingNode node2)
    {
        if (ReferenceEquals(node1, null) || ReferenceEquals(node2, null))
            return false;
        return !node1.Equals(node2);
    }
}
