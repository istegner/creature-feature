﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementMotor : MonoBehaviour
{
    public bool debug;
    public float walkSpeed;
    public float runSpeed;
    public float R;
    public float dotDistance;
    public int nodesToSkip;
    [Space()]

    public PathManager pathManager;
    public List<PathDataNode> path;
    public Rigidbody rb;
    public Vector3 target;

    public bool navigating = false;
    GameObject ground;

    private void Start()
    {
        if (pathManager == null)
            pathManager = FindObjectOfType<PathManager>();
        if (rb == null)
            rb = GetComponentInChildren<Rigidbody>();
        if (ground == null)
            ground = GameObject.FindGameObjectWithTag("Ground");
    }

    private void FixedUpdate()
    {
        if (navigating && path?.Count > 0)
        {
            var newPos = path[0].worldPos;
            var newVec = Vector3.Lerp(rb.velocity, (newPos - transform.position).normalized * walkSpeed, R * Time.deltaTime);
            //TODO: Potential fields
            transform.forward = newVec.normalized;
            rb.velocity = newVec;

            //Check if we have passed the node already
            if (path.Count >= 2)
            {
                var vecNext = path[1].worldPos - path[0].worldPos;
                var vecCurr = transform.position - path[0].worldPos;
                var dot = Vector3.Dot(vecNext, vecCurr);
                if (dot > 0 && Vector3.Distance(transform.position, path[0].worldPos) < dotDistance)
                    RemoveNode(0);
            }
            //If we're close enough to the node, remove it from the path
            var distance = Vector3.Distance(transform.position, newPos);
            if (distance < 0.1f)
            {
                RemoveNode(0);
                SkipNodes();
            }
        }
        else
        {
            rb.velocity = Vector3.zero;
            navigating = false;
        }
    }

    /// <summary>
    /// Get a path to a random point to move to
    /// </summary>
    public void GetPath()
    {
        GetPath(RandomPoint());
    }

    /// <summary>
    /// Get a path to pos to move to
    /// </summary>
    /// <param name="pos"></param>
    public void GetPath(Vector3 pos)
    {
        ClearPath();
        target = pos;
        var time = Time.realtimeSinceStartup;
        path = pathManager.GeneratePath(transform.position, pos);
        if (path != null)
        {
            if (path.Count > 0 && !navigating)
            {
                navigating = true;
                for (int i = 0; i < path.Count; i++)
                {
                    path[i].sphere.GetComponent<Renderer>().material.color = Color.blue;
                }
            }
        }
        time = Time.realtimeSinceStartup - time;
        if (debug) Debug.Log(gameObject.name + " took " + time + " to generate path", gameObject);
    }

    void RemoveNode(int index)
    {
        path[index].sphere.GetComponent<Renderer>().material.color = Color.white;
        path.Remove(path[index]);
    }

    /// <summary>
    /// Get a random point on the world to move to
    /// </summary>
    /// <param name="bounds"></param>
    /// <returns></returns>
    public Vector3 RandomPoint()
    {
        Bounds bounds = ground.GetComponent<MeshCollider>().bounds;
        var randPoint = new Vector3(Random.Range(bounds.min.x, bounds.max.x), 0.5f, Random.Range(bounds.min.z, bounds.max.z));
        if (pathManager.WorldToNode(randPoint).walkable)
            return randPoint;
        return RandomPoint();
    }

    /// <summary>
    /// See if we can skip some nodes
    /// </summary>
    void SkipNodes()
    {
        var highest = 0;
        if (path.Count >= nodesToSkip)
        {
            for (int i = 1; i < nodesToSkip; i++)
            {
                if (path[i] != null)
                {
                    var rayOrigin = transform.position;
                    var heading = path[i].worldPos - rayOrigin;
                    var distance = heading.magnitude;
                    var direction = heading / distance;
                    RaycastHit hit;
                    if (Physics.Raycast(rayOrigin, direction, out hit, distance))
                    {
                        break;
                    }
                    else
                    {
                        highest++;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        for (int i = 0; i < highest - 1; i++)
        {
            path[0].sphere.GetComponent<Renderer>().material.color = Color.white;
            path.Remove(path[0]);
        }
    }

    /// <summary>
    /// Clear the current path
    /// </summary>
    public void ClearPath()
    {
        while(path?.Count > 0)
        {
            RemoveNode(0);
        }
    }

    /// <summary>
    /// Every 2 seconds see if we're doing something and if not, find a path
    /// </summary>
    /// <returns></returns>
    IEnumerator GetWaypoint()
    {
        yield return new WaitForSeconds(2);
        if (path.Count == 0 && !navigating)
            GetPath();
        StartCoroutine(GetWaypoint());
    }
}
