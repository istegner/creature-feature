﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vitals : MonoBehaviour
{
    public int foodLevel;
    public float timeToLive;
    public float timeAlive;

    private void Start()
    {
        timeAlive = 0.0f;
    }

    private void Update()
    {
        timeAlive += Time.deltaTime;
        if (timeAlive >= timeToLive)
        {
            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
