﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogSpawner : MonoBehaviour
{
    public bool debug = true;
    public PathManager pathManager;
    public GameSettings gameSettings;

    public float scale = 0.05f;
    public float offsetX = 100f;
    public float offsetY = 100f;

    public GameObject frogPrefab;
    public Transform frogParent;

    public int frogs;

    private void OnEnable()
    {
        EventManager.OnNodesGenerated += SpawnFrogs;
    }

    private void OnDisable()
    {
        EventManager.OnNodesGenerated -= SpawnFrogs;
    }

    void SpawnFrogs()
    {
        if (debug) Debug.Log("Spawning frogs");
        frogs = 0;
        if (pathManager == null)
            pathManager = FindObjectOfType<PathManager>();
        for (int i = 0; i < pathManager.pathDataNodes.Count; i++)
        {
            var node = pathManager.pathDataNodes[i];
            if (node.walkable)
            {
                var chance = CalculateChance(node.worldPos.x, node.worldPos.y);
                if (chance < gameSettings.frogSpawnChance)
                {
                    if (gameSettings.frogDensity > Random.Range(0f, 1f))
                    {
                        SpawnFrog(node.worldPos);
                    }
                }
            }
        }
        if (debug) Debug.Log("Done spawning frogs. Spawned " + frogs + " frogs");
        EventManager.FrogsHaveBeenSpawned();
    }

    public void SpawnFrog(Vector3 pos)
    {
        var newFrog = Instantiate(frogPrefab, pos, frogPrefab.transform.rotation, frogParent);
        newFrog.name = "Frog" + frogs;
        frogs++;
    }

    float CalculateChance(float x, float y)
    {
        float xCoord = (x + offsetX) / (pathManager.width * scale);
        float yCoord = (y + offsetY) / (pathManager.height * scale);
        return Mathf.PerlinNoise(xCoord, yCoord);
    }
}
