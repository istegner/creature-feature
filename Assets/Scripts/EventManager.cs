﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public delegate void WorldGeneration();
    public static event WorldGeneration OnNodesGenerated;
    public static event WorldGeneration OnFrogsSpawned;

    public static void NodesHaveBeenGenerated()
    {
        if (OnNodesGenerated != null)
            OnNodesGenerated();
    }

    public static void FrogsHaveBeenSpawned()
    {
        if (OnFrogsSpawned != null)
            OnFrogsSpawned();
    }
}
