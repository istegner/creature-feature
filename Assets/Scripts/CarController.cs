﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(BehaviourController), typeof(MovementMotor))]
public class CarController : MonoBehaviour
{
    public bool debug;
    public float maxChaseDistance;
    public float maxChaseTime;
    public float currentChaseTime;
    Rigidbody rb;
    private MovementMotor movementMotor;
    private BehaviourController behaviourController;
    private PathAttendant pathAttendant;
    public bool overrideMovementMotor;
    float speed;
    float r;
    public float catcherResetTime;
    public bool canCatch = true;

    [Space]
    public int width = 128;
    public int height = 48;
    public RenderTexture rt;
    public Texture2D texture;
    public Camera carCam;
    public float rotationScale;
    public float overallForce;
    public float[] colourArray;

    // Start is called before the first frame update
    void Start()
    {
        if (movementMotor == null)
            movementMotor = GetComponent<MovementMotor>();
        if (behaviourController == null)
            behaviourController = GetComponent<BehaviourController>();
        pathAttendant = FindObjectOfType<PathAttendant>();
        rb = GetComponent<Rigidbody>();
        speed = movementMotor.walkSpeed;
        r = movementMotor.R;

        rt = new RenderTexture(width, height, 24);
        carCam.targetTexture = rt;
        texture = new Texture2D(width, height, TextureFormat.RGB24, false);
    }

    // Update is called once per frame
    void Update()
    {
        if (ProbeCamera())
        {
            overrideMovementMotor = true;
            behaviourController.Chase(null);
        }
        else
        {
            overrideMovementMotor = false;
            behaviourController.Idle();
        }

        if (overrideMovementMotor && movementMotor.enabled)
        {
            movementMotor.enabled = false;
            movementMotor.ClearPath();
        }
        else if (!overrideMovementMotor && !movementMotor.enabled)
            movementMotor.enabled = true;
        else if (!overrideMovementMotor && movementMotor.enabled && !movementMotor.navigating)
            pathAttendant.AddToQueue(gameObject, movementMotor.RandomPoint());

        if (behaviourController.IsChasing())
        {
            var desVel = (transform.forward + (transform.right * overallForce * rotationScale));
            desVel.y = 0.0f;
            transform.position = new Vector3(transform.position.x, 0.56f, transform.position.z);
            desVel.Normalize();
            var newVec = Vector3.Lerp(rb.velocity, desVel * speed, r * Time.deltaTime);
            //TODO: Potential fields
            transform.forward = newVec.normalized;
            rb.velocity = newVec;
        }
    }

    bool ProbeCamera()
    {
        var prevState = RenderTexture.active;
        RenderTexture.active = rt;
        texture.ReadPixels(new Rect(0, 0, width, height), 0, 0, false);
        texture.Apply();

        colourArray = new float[texture.width];
        overallForce = 0.0f;
        int itt = 0;
        for (int i = 0; i < colourArray.Length; i++)
        {
            var colours = texture.GetPixels(i, 0, 1, texture.height);
            float average = 0.0f;
            int it = 0;
            for (int j = 0; j < colours.Length; j++)
            {
                var colour = colours[j];
                if (colour.r == 0 && colour.g == 0 && colour.b == 0)
                    continue;
                else
                {
                    it++;
                    float h, s, v;
                    Color.RGBToHSV(colour, out h, out s, out v);
                    average += h;
                }
            }
            if (it > 0)
            {
                //average /= it;
                float relativePos = ((float)i / ((texture.width - 1) / 2)) - 1.0f;
                var force = relativePos * average;
                overallForce += force;
                itt++;
            }
            colourArray[i] = average;
        }
        if (itt > 0)
            overallForce /= itt;

        RenderTexture.active = prevState;

        if (itt > 0)
            return true;
        return false;
    }

    void StopChasing()
    {
        overrideMovementMotor = false;
        behaviourController.Chase(null);
        behaviourController.Idle();
    }

    private void OnTriggerStay(Collider other)
    {
        if (canCatch && other.gameObject.tag.Contains("Toad") && behaviourController != null)
        {
            if (debug) Debug.Log(gameObject.name + " caught " + other.transform.parent.gameObject.name);
            Destroy(other.transform.parent.gameObject);
            canCatch = false;
            StartCoroutine(ResetCatcher());
        }
    }

    IEnumerator ResetCatcher()
    {
        yield return new WaitForSeconds(catcherResetTime);
        canCatch = true;
    }
}
