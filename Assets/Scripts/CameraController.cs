﻿using UnityEngine;

/// <summary>
/// Allows for camera movement using horizontal and vertical axis and for rotation around a pivot point
/// Place this script on the Main Camera, which should be a child of an empty GO which will act as the pivot point
/// </summary>
public class CameraController : MonoBehaviour
{
    public PathManager pathManager;

    [Space]
    [Tooltip("How sensitive to rotate")]
    public float mouseSensitivity = 4f;

    [Tooltip("How sensitive to scroll")]
    public float scrollSensitivity = 2f;

    [Space]
    [Tooltip("How much smoothing to add to rotating")]
    public float orbitDampening = 10f;

    [Tooltip("How much smoothing to add to zooming")]
    public float scrollDampening = 6f;

    [Space]
    [Tooltip("How close to allow the camera to zoom to the pivot point")]
    public float minZoom = 2.5f;

    [Tooltip("How far to allow the camera to zoom from the pivot point")]
    public float maxZoom = 100f;

    [Space]
    [Tooltip("The min angle the camera will rotate to")]
    public float minAngle = 5;

    [Tooltip("The max angle the camera will rotate to")]
    public float maxAngle = 90;

    [Space]
    [Tooltip("How fast to move the camera")]
    public float moveSpeed = 100;

    [HideInInspector] public float cameraDistance = 10f; //How far is the camera from the pivot point

    private bool lockCamera = false; //When true, camera will not rotate and cursor will show
    private Vector3 localRotation;
    private Camera playerCamera;
    private Transform cameraPivot; //The cameras pivot point (the parent)

    private bool followMode;
    private GameObject following; //What are we following
    private Vector3 vel;
    [Space]
    public float followHeight;
    public float followDistance;
    public float smooth;
    public int maxGraceFrames; //Give the player leeway if they're moving the camera to catch what they want to follow
    private int graceFramesPast;

    [Space]
    public UIController uiController;

    // Use this for initialization
    private void Start()
    {
        if (pathManager == null)
            pathManager = FindObjectOfType<PathManager>();
        if (uiController == null)
            uiController = FindObjectOfType<UIController>();
        playerCamera = GetComponent<Camera>();
        cameraPivot = transform.parent;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        // Check if we click on something followable and follow it, showing its stats
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag.Contains("Followable"))
                {
                    graceFramesPast = 0;
                    cameraDistance = followHeight;
                    followMode = true;
                    following = hit.transform.gameObject;

                    if (hit.transform.gameObject.name.Contains("Frog") || hit.transform.gameObject.name.Contains("Toad"))
                    {
                        uiController.ShowFrogStats(hit.transform.gameObject);
                    }
                    else if (hit.transform.parent.gameObject.name.Contains("Car"))
                    {
                        uiController.ShowCarStats(hit.transform.gameObject);
                    }
                }
            }
        }

        //keep the camera on the GO we're following
        if (followMode && following != null)
        {
            var targetPos = following.transform.TransformPoint(new Vector3(0, followHeight, -followDistance));
            cameraPivot.position = Vector3.SmoothDamp(cameraPivot.position, following.transform.position, ref vel, smooth);
        }
        graceFramesPast++;
    }

    // Late Update is called once per frame, after Update() on every game object in the scene.
    private void LateUpdate()
    {
        //Lock/Unlock the cursor
        if (Input.GetKeyDown(KeyCode.L))
        {
            ToggleCursor();
        }

        //If the camera is moveable
        if (!lockCamera)
        {
            //Get the mouse input for rotation
            localRotation.x += Input.GetAxis("Mouse X") * mouseSensitivity;
            localRotation.y -= Input.GetAxis("Mouse Y") * mouseSensitivity;

            //Clamp the y rotation to horizon and not flipping over at top
            localRotation.y = Mathf.Clamp(localRotation.y, minAngle, maxAngle);

            //Zooming input from our mouse scroll wheel
            var scrollAmount = Input.GetAxis("Mouse ScrollWheel") * scrollSensitivity;

            //Makes camera zoom faster the further away it is from the target
            scrollAmount *= (cameraDistance * 0.3f);
            cameraDistance += scrollAmount * -1f;

            //This makes camer go no closer than minZoom from target and no further than maxZoom.
            cameraDistance = Mathf.Clamp(cameraDistance, minZoom, maxZoom);
        }

        ///Camera movement///
        var movHori = Input.GetAxisRaw("Horizontal") * cameraPivot.right;
        Vector3 movVert;

        /*If the camera pivot's rotation on the x-axis is more than 45, use transform.up
        This stops the camera movement forward/back stopping when looking straight down due to
        transform.forward being straight down and transform.up now being "forward"*/
        if (cameraPivot.rotation.x < 45)
            movVert = Input.GetAxisRaw("Vertical") * cameraPivot.up;
        else
            movVert = Input.GetAxisRaw("Vertical") * cameraPivot.forward;

        //Stop following if movement keys pressed
        if (movHori.magnitude > 0 || movVert.magnitude > 0)
        {
            if (graceFramesPast >= maxGraceFrames)
            {
                followMode = false;
                following = null;
                uiController.HideStats();
            }
        }

        //Get the new position of the camera                                                               Make the movement faster the further zoomed the camera is
        var newPos = cameraPivot.position + (movVert + movHori).normalized * ((moveSpeed * Time.deltaTime) * (cameraDistance / maxZoom));
        //Clamp the camera to the path bounds
        newPos.x = Mathf.Clamp(newPos.x, pathManager.startPoint.x, pathManager.endPoint.x);
        newPos.z = Mathf.Clamp(newPos.z, pathManager.startPoint.z, pathManager.endPoint.z);
        //Keep the camera at the same y pos, we only want to change height when scrolling
        newPos.y = cameraPivot.position.y;
        //Move the camera
        cameraPivot.position = newPos;

        //Actual camer rig transformations
        var rot = Quaternion.Euler(localRotation.y, localRotation.x, 0);
        cameraPivot.rotation = Quaternion.Lerp(cameraPivot.rotation, rot, Time.deltaTime * orbitDampening);

        if (playerCamera.transform.localPosition.z != cameraDistance * -1f)
        {
            playerCamera.transform.localPosition = new Vector3(0f, 0f, Mathf.Lerp(playerCamera.transform.localPosition.z, cameraDistance * -1f, Time.deltaTime * scrollDampening));
        }
    }

    /// <summary>
    /// Enable/Disable the cursor
    /// </summary>
    public void ToggleCursor()
    {
        lockCamera = !lockCamera;
        Cursor.visible = !Cursor.visible;
        //If the cursor in locked, unlock it, else, lock it
        Cursor.lockState = Cursor.lockState == CursorLockMode.Locked ? CursorLockMode.None : CursorLockMode.Locked;
    }

    public void EnableCursor(bool on)
    {
        if (lockCamera == on)
            return;
        else
            ToggleCursor();
    }
}