﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameSettings : ScriptableObject
{
    public float frogSpawnChance;
    public float frogDensity;
    public float toadSpawnChance;
    public float toadDensity;
    //public float foodSpawnChance;
    public float foodDensity;
    public int numRCCars;

    public bool startFromLoading;
}
