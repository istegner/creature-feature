﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodController : MonoBehaviour
{
    public bool eatable;
    public GameObject pond;
    public float respawnTime;
    float actualRespawnTime;
    private Collider _collider;
    private Renderer _renderer;

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<Collider>();
        _renderer = GetComponent<Renderer>();
        Respawn();
        actualRespawnTime = respawnTime * ((respawnTime + Vector3.Distance(transform.position, pond.transform.position)) / respawnTime);
    }

    public void Respawn()
    {
        _collider.enabled = true;
        _renderer.enabled = true;
        eatable = true;
    }

    public void GetEaten()
    {
        _collider.enabled = false;
        _renderer.enabled = false;
        eatable = false;
        StartCoroutine(CheckRespawn());
    }

    IEnumerator CheckRespawn()
    {
        yield return new WaitForSeconds(actualRespawnTime);
        if (!eatable)
        {
            Respawn();
        }
    }
}
