﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathManager : MonoBehaviour
{
    public bool debug = true; //Show debugging stuff
    public bool showWireSpheres = true;

    public GameSettings gameSettings;

    public List<PathDataNode> pathDataNodes; //All the nodes

    public Vector3 startPoint;
    public Vector3 endPoint;
    public float stepDistance;
    public LayerMask unwalkableMask;

    bool showNodesInGame = true;
    public GameObject sphere;
    public Material white;
    public Material red;
    public Material blue;
    public GameObject sphereParent; //Where to put the spheres in the hierarchy

    public List<PathDataNode> path; //The generated path

    public int width
    {
        get
        {
            return pathDataNodes.Count / numPathdataRows;
        }
    }

    public int height
    {
        get
        {
            return numPathdataRows;
        }
    }

    public Vector2 worldSize
    {
        get
        {
            var x = Mathf.Abs(endPoint.x - startPoint.x);
            var y = Mathf.Abs(endPoint.y - startPoint.y);
            return new Vector2(x, y);
        }
    }

    private int nodes = 0, voidNodes = 0, walkableNodes = 0, unwalkableleNodes = 0;
    private float timeTaken = 0.0f;

    private int numPathdataRows = 0;

    // Start is called before the first frame update
    void Start()
    {
        //Generate all the path nodes
        if (debug) Debug.Log("Generating path data");
        timeTaken = Time.realtimeSinceStartup;
        CreatePathData();
        timeTaken = Time.realtimeSinceStartup - timeTaken;
        if (debug) Debug.Log("Finished generating path data in " + timeTaken + " seconds. Created " + nodes + " nodes with " + voidNodes + " not over a mesh, " + walkableNodes + " walkable and " + unwalkableleNodes + " unwalkable.");
        if (gameSettings.startFromLoading)
        {
            gameSettings.startFromLoading = false;
            FindObjectOfType<DisableCollider>().DisableColliders();
            FindObjectOfType<GameLoader>().LoadGame();
            FindObjectOfType<FoodManager>().SpawnFood();
        }
        else
            EventManager.NodesHaveBeenGenerated();
    }

    void CreatePathData()
    {
        numPathdataRows = 0;
        for (float z = startPoint.z; z < endPoint.z; z += stepDistance)
        {
            ++numPathdataRows;
            for (float x = startPoint.x; x < endPoint.x; x += stepDistance)
            {
                //Raycast from the top most point down
                Vector3 rayOrigin = new Vector3(x, endPoint.y, z);
                RaycastHit hit;
                //If raycast hit something
                if (Physics.Raycast(rayOrigin, Vector3.down, out hit))
                {
                    var point = hit.point;
                    point.y += stepDistance / 2;
                    //Check the immediate area around what was hit to see if it will clip through anything that's unwalkable
                    bool walkable = !(Physics.CheckSphere(new Vector3(point.x, point.y + (stepDistance), point.z), stepDistance * 2, unwalkableMask));
                    CreateNode(walkable, point);
                    if (walkable)
                        walkableNodes++;
                    else
                        unwalkableleNodes++;
                }
                //Raycast didnt hit. Create a node anyway at the lowest point
                else
                {
                    CreateNode(false, new Vector3(x, startPoint.y, z));
                    voidNodes++;
                    unwalkableleNodes++;
                }
            }
        }
    }

    /// <summary>
    /// Create a new node
    /// </summary>
    /// <param name="walk"></param>
    /// <param name="pos"></param>
    void CreateNode(bool walk, Vector3 pos)
    {
        var newNode = new PathDataNode(nodes, pos, walk);
        //Create a sphere so the node can be seen in the game window
        var _sphere = Instantiate(sphere, pos, sphere.transform.rotation, sphereParent.transform);
        //Colour the sphere based on its walkable state
        _sphere.GetComponent<Renderer>().material = walk ? white : red;
        //Disable it because the gizmos spheres are on by default
        _sphere.SetActive(false);
        newNode.sphere = _sphere;
        pathDataNodes.Add(newNode);
        nodes++;
    }

    /// <summary>
    /// Generate a new path with world positions as start/end point
    /// </summary>
    public List<PathDataNode> GeneratePath(Vector3 startPos, Vector3 endPos)
    {
        return GeneratePath(WorldToNode(startPos), WorldToNode(endPos));
    }

    /// <summary>
    /// Generate a new path with PathDataNodes as start/end points
    /// </summary>
    /// <param name="startNode"></param>
    /// <param name="endNode"></param>
    public List<PathDataNode> GeneratePath(PathDataNode startNode, PathDataNode endNode)
    {
        //The open/closed lists
        List<PathFindingNode> openList = new List<PathFindingNode>();
        List<PathFindingNode> closedList = new List<PathFindingNode>();

        bool[] openFlags = new bool[pathDataNodes.Count];
        bool[] closedFlags = new bool[pathDataNodes.Count];
        for (int i = 0; i < openFlags.Length; i++)
        {
            openFlags[i] = closedFlags[i] = false;
        }

        //Create the PathFinding nodes for the start/end nodes
        var _startNode = new PathFindingNode(startNode);
        var _endNode = new PathFindingNode(endNode);
        //Add the start node to the open list
        openList.Add(_startNode);
        openFlags[startNode.index] = true;
        _startNode.g = 0;
        _startNode.h = GetDistance(startNode, endNode);
        //While the open list is not empty
        while (openList.Count > 0)
        {
            //Get the node in the open list with the lowest f-cost or h-cost if multiple have the same f-cost
            var currentNode = openList[0];
            for (int i = 1; i < openList.Count; i++)
            {
                if (openList[i].f < currentNode.f || (openList[i].f == currentNode.f && (openList[i].h < currentNode.h)))
                {
                    currentNode = openList[i];
                }

            }
            //Remove the current node from the open list and add it to the closed list
            openList.Remove(currentNode);
            closedList.Add(currentNode);
            openFlags[currentNode.pathdataNode.index] = false;
            closedFlags[currentNode.pathdataNode.index] = true;

            //If the current node is the end node, we're done
            if (currentNode == _endNode)
            {
                RetracePath(_startNode, currentNode);
                return path;
            }

            //Get the neighbour nodes and cycle through them
            foreach (PathFindingNode neighbour in GetNeighbours(currentNode, openList, openFlags, closedFlags))
            {
                //If the neighbour is in the closed list, it's already been evaluated and deemed useless.
                if (closedFlags[neighbour.pathdataNode.index])
                    continue;
                //Get how much it will cost to move to the neighbour
                float dist = GetDistance(currentNode.pathdataNode, neighbour.pathdataNode);
                float costToNeighbour = currentNode.g + dist;

                //If it costs less to get to the neighbour from where we currently are or its cost is 0
                //Set its g and h cost based on where we currently are and add it to the open list if its not already
                if (costToNeighbour < neighbour.g || !openFlags[neighbour.pathdataNode.index])
                {
                    neighbour.g = costToNeighbour;
                    neighbour.h = GetDistance(neighbour, _endNode);
                    neighbour.parentNode = currentNode;

                    if (!openFlags[neighbour.pathdataNode.index])
                    {
                        openList.Add(neighbour);
                        openFlags[neighbour.pathdataNode.index] = true;
                    }
                }
            }
        }
        if (debug) Debug.LogWarning("Path Gen Failed from " + startNode.worldPos + " to " + endNode.worldPos, endNode.sphere);
        return null;
    }

    /// <summary>
    /// Get the path from start to end by tracing the parent nodes starting at the end
    /// then reverse it to get it the correct direction
    /// </summary>
    void RetracePath(PathFindingNode start, PathFindingNode end)
    {
        path = new List<PathDataNode>();
        var currentNode = end;
        while (currentNode != start)
        {
            path.Add(currentNode.pathdataNode);
            currentNode = currentNode.parentNode;
        }
        path.Reverse();
    }

    /// <summary>
    /// Get the nodes neighbouring the node 'node'
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    public List<PathFindingNode> GetNeighbours(PathFindingNode node, List<PathFindingNode> openList, bool[] openFlags, bool[] closedFlags)
    {
        //The list of neighbours to return
        var neighbours = new List<PathFindingNode>();
        //The index of the current node
        var index = node.pathdataNode.index;
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                //This will give back the current node so ignore it
                if (x == 0 && y == 0)
                    continue;
                //The grid corordinate of the neighbour
                Vector2Int gridCoord = IndexToGrid(index) + new Vector2Int(x, y);
                //Check if the coord is valid (on the grid)
                if (gridCoord.x >= width || gridCoord.x < 0 || gridCoord.y >= height || gridCoord.y < 0)
                    continue;
                //Check if the node is walkable; don't want to check something that we can't even walk on
                else if (pathDataNodes[GridToIndex(gridCoord)].walkable)
                {
                    bool foundTheThing = false;

                    int searchIndex = GridToIndex(gridCoord);
                    if (closedFlags[searchIndex])
                        continue;
                    else if (openFlags[searchIndex])
                    {
                        foreach (PathFindingNode openNode in openList)
                        {
                            if (openNode.pathdataNode.index == searchIndex)
                            {
                                foundTheThing = true;
                                neighbours.Add(openNode);

                                break;
                            }
                        }
                    }

                    if (foundTheThing)
                        continue;

                    var neighbour = new PathFindingNode(pathDataNodes[GridToIndex(gridCoord)]);
                    neighbours.Add(neighbour);
                }
            }
        }
        return neighbours;
    }

    /// <summary>
    /// Convert a world position to the nearest node
    /// </summary>
    /// <param name="worldPos"></param>
    /// <returns></returns>
    public PathDataNode WorldToNode(Vector3 worldPos)
    {
        //Get how far on the x/z-axis the pos is as a percent                                                    -1 here because indexing starts at 0
        var percentX = Mathf.Clamp01((worldPos.x - pathDataNodes[0].worldPos.x) / pathDataNodes[pathDataNodes.Count - 1].worldPos.x);
        var percentZ = Mathf.Clamp01((worldPos.z - pathDataNodes[0].worldPos.z) / pathDataNodes[pathDataNodes.Count - 1].worldPos.z);
        //                              -1 here because indexing starts at 0
        var x = Mathf.FloorToInt((width - 1) * percentX);
        var y = Mathf.FloorToInt((height - 1) * percentZ);
        return pathDataNodes[GridToIndex(new Vector2Int(x, y))];
    }

    /// <summary>
    /// 1D to 2D conversion
    /// Get the 2D grid position based on an index
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    Vector2Int IndexToGrid(int index)
    {
        return new Vector2Int(index % (int)width, index / (int)width);
    }

    /// <summary>
    /// 2D to 1D conversion
    /// Get the index based on a grid position
    /// </summary>
    /// <param name="grid"></param>
    /// <returns></returns>
    int GridToIndex(Vector2Int grid)
    {
        return grid.x + (grid.y * width);
    }

    /// <summary>
    /// Get the distance between 2 nodes
    /// </summary>
    /// <param name="pointA"></param>
    /// <param name="pointB"></param>
    /// <returns></returns>
    float GetDistance(PathFindingNode pointA, PathFindingNode pointB)
    {
        return GetDistance(pointA.pathdataNode, pointB.pathdataNode);
    }

    float GetDistance(PathDataNode pointA, PathDataNode pointB)
    {
        var distX = Mathf.Abs(pointA.worldPos.x - pointB.worldPos.x);
        var distY = Mathf.Abs(pointA.worldPos.z - pointB.worldPos.z);
        distX *= distX;
        distY *= distY;
        return Mathf.Sqrt(distX + distY);
    }

    /// <summary>
    /// Show the nodes in the game window instead of using gizmos
    /// </summary>
    public void ShowNodesInGame()
    {
        showNodesInGame = !showNodesInGame;
        if (sphereParent.transform.childCount > 0)
        {
            foreach (Transform child in sphereParent.transform)
            {
                child.gameObject.SetActive(showNodesInGame);
            }
        }
    }

    /// <summary>
    /// If debug is true and the node spheres are disabled
    /// Show the nodes using gizmos coloured based on walkable state and if it's part of a path
    /// </summary>
    private void OnDrawGizmos()
    {
        if (debug && !showNodesInGame && showWireSpheres)
        {
            foreach (PathDataNode n in pathDataNodes)
            {
                Gizmos.color = n.walkable ? Color.white : Color.red;
                if (path.Count > 0)
                    if (path.Contains(n))
                        Gizmos.color = Color.black;
                Gizmos.DrawWireSphere(n.worldPos, 0.05f);
            }
        }
    }

    private void OnValidate()
    {
        if (!debug)
            showWireSpheres = false;
    }
}
