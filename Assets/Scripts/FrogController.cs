﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(BehaviourController), typeof(MovementMotor))]
[RequireComponent(typeof(Vitals))]
public class FrogController : MonoBehaviour
{
    MovementMotor movementMotor;
    BehaviourController behaviourManager;
    Vitals vitals;
    public int minFoodToMate;
    public int maxFoodToMate;
    [Space] public int foodToMate;

    [SerializeField] bool waitingForFood = false;

    private void OnEnable()
    {
        if (movementMotor == null)
            movementMotor = GetComponent<MovementMotor>();
        if (behaviourManager == null)
            behaviourManager = GetComponent<BehaviourController>();
        if (vitals == null)
            vitals = GetComponent<Vitals>();
        vitals.foodLevel = 0;
        foodToMate = Random.Range(minFoodToMate, maxFoodToMate);
    }

    private void OnDisable()
    {
        movementMotor.ClearPath();
        behaviourManager?.matingManger?.FrogDied(gameObject);
    }

    private void Update()
    {
        if (behaviourManager.IsIdling() && !waitingForFood)
        {
            StartCoroutine(LookForFood());
        }

        if (movementMotor.path?.Count == 0)
        {
            if (behaviourManager.IsLookingForFood())
            {
                if (behaviourManager.currentFood != null)
                {
                    if (Vector3.Distance(transform.position, behaviourManager.currentFood.transform.position) < 0.3f)
                        Eat(behaviourManager.currentFood);
                }
            }
            else if (behaviourManager.IsLookingForMate() && !behaviourManager.waiter)
            {
                if (behaviourManager.mate != null)
                {
                    var dist = Vector3.Distance(transform.position, behaviourManager.mate.transform.position);
                    if (dist < 0.2f)
                    {
                        if (behaviourManager.isFrog)
                            behaviourManager.matingManger.MateFrog(gameObject);
                        else if (behaviourManager.isToad)
                            behaviourManager.matingManger.MateToad(gameObject);
                    }
                }
            }
            movementMotor.navigating = false;
        }
    }

    IEnumerator LookForFood()
    {
        waitingForFood = true;
        yield return new WaitForSeconds(Random.Range(0, 10));
        behaviourManager.LookForFood();
        waitingForFood = false;
    }

    void Eat(GameObject toEat)
    {
        vitals.foodLevel++;
        vitals.timeAlive -= 10.0f;
        behaviourManager?.foodManager?.RemoveFood(toEat);
        behaviourManager.currentFood = null;
        behaviourManager?.Idle();
        StartCoroutine(LookForFood());
        if (vitals.foodLevel >= foodToMate)
        {
            behaviourManager.LookForMate();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (behaviourManager.isToad)
        {
            if (other.CompareTag("Food"))
            {
                Eat(other.gameObject);
            }
        }
    }
}
