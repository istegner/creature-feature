﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public TMP_Text gameName;
    public TMP_Text madeBy;
    public TMP_Text version;

    public GameSettings defaultSettings;
    public GameSettings gameSettings;
    public Slider frogPop;
    public Slider toadPop;
    public Slider foodDensity;
    public TMP_InputField numRcCars;

    // Start is called before the first frame update
    void Start()
    {
        gameName.text = Application.productName;
        madeBy.text = Application.companyName;
        version.text = "v" + Application.version;
        ResetToDefaults();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void StartGame()
    {
        gameSettings.frogSpawnChance = frogPop.value;
        gameSettings.toadSpawnChance = toadPop.value;
        gameSettings.foodDensity = foodDensity.value;
        int numCars;
        if (int.TryParse(numRcCars.text, out numCars))
            gameSettings.numRCCars = numCars;
        else
            gameSettings.numRCCars = 1;
        SceneManager.LoadScene(1);
    }

    public void ResetToDefaults()
    {
        frogPop.value = defaultSettings.frogSpawnChance;
        toadPop.value = defaultSettings.toadSpawnChance;
        foodDensity.value = defaultSettings.foodDensity;
        numRcCars.text = defaultSettings.numRCCars.ToString();
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
