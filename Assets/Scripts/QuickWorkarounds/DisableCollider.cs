﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableCollider : MonoBehaviour
{
    private void OnEnable()
    {
        EventManager.OnNodesGenerated += DisableColliders;
    }

    private void OnDisable()
    {
        EventManager.OnNodesGenerated -= DisableColliders;
    }

    public void DisableColliders()
    {
        foreach(Transform child in transform)
        {
            child.GetComponent<Collider>().enabled = false;
        }
    }
}
