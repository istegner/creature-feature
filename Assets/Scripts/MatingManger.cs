﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatingManger : MonoBehaviour
{
    public bool debug;
    public GameObject frogPrefab;
    public Transform frogParent;
    public GameObject toadPrefab;
    public Transform toadParent;
    public List<GameObject> frogsReadyToMate;
    public List<GameObject> toadsReadyToMate;

    List<GameObject> frogsMating;
    List<GameObject> toadsMating;

    FrogSpawner frogSpawner;
    ToadSpawner toadSpawner;

    private void Start()
    {
        if (frogSpawner == null)
            frogSpawner = FindObjectOfType<FrogSpawner>();
        toadSpawner = FindObjectOfType<ToadSpawner>();
        frogsMating = new List<GameObject>();
        toadsMating = new List<GameObject>();
    }

    private void Update()
    {
        if (frogsReadyToMate.Count > 1)
        {
            if (frogsReadyToMate[0] == null)
            {
                frogsReadyToMate.RemoveAt(0);
                return;
            }
            AssignMates_Frogs(frogsReadyToMate[0], frogsReadyToMate[1]);
        }
        if (toadsReadyToMate.Count > 1)
        {
            if (toadsReadyToMate[0] == null)
            {
                toadsReadyToMate.RemoveAt(0);
                return;
            }
            AssignMates_Toads(toadsReadyToMate[0], toadsReadyToMate[1]);
        }
    }

    void AssignMates_Frogs(GameObject mate1, GameObject mate2)
    {
        var bm1 = mate1.GetComponent<BehaviourController>();
        var bm2 = mate2.GetComponent<BehaviourController>();
        bm1.mate = mate2;
        bm2.mate = mate1;
        frogsMating.Add(mate1);
        frogsMating.Add(mate2);
        frogsReadyToMate.Remove(mate1);
        frogsReadyToMate.Remove(mate2);
        bm1.waiter = true;
        bm2.waiter = false;
        bm1.waitingForMate = false;
        bm2.waitingForMate = false;
        if (debug) Debug.Log("Assigned " + mate1.name + " to mate with " + mate2.name);
    }

    void AssignMates_Toads(GameObject mate1, GameObject mate2)
    {
        var bm1 = mate1.GetComponent<BehaviourController>();
        var bm2 = mate2.GetComponent<BehaviourController>();
        bm1.mate = mate2;
        bm2.mate = mate1;
        toadsMating.Add(mate1);
        toadsMating.Add(mate2);
        toadsReadyToMate.Remove(mate1);
        toadsReadyToMate.Remove(mate2);
        bm1.waiter = true;
        bm2.waiter = false;
        bm1.waitingForMate = false;
        bm2.waitingForMate = false;
        if (debug) Debug.Log("Assigned " + mate1.name + " to mate with " + mate2.name);
    }

    public void MateFrog(GameObject frog)
    {
        var newFrog = Instantiate(frogPrefab, frog.transform.position, frogPrefab.transform.rotation, frogParent);
        newFrog.name = "Frog" + frogSpawner.frogs;
        frogSpawner.frogs++;
        var bm1 = frog.GetComponent<BehaviourController>();
        var bm2 = frog.GetComponent<BehaviourController>().mate.GetComponent<BehaviourController>();
        frogsMating.Remove(frog);
        frogsMating.Remove(bm1.mate);
        bm1.mate = null;
        bm2.mate = null;
        bm1.waiter = false;
        bm2.waiter = false;
        bm1.GetComponent<FrogController>().GetComponent<Vitals>().foodLevel = 0;
        bm2.GetComponent<FrogController>().GetComponent<Vitals>().foodLevel = 0;
        bm1.Idle();
        bm2.Idle();
    }

    public void MateToad(GameObject toad)
    {
        var newToad = Instantiate(toadPrefab, toad.transform.position, toadPrefab.transform.rotation, toadParent);
        newToad.name = "Toad" + toadSpawner.toads;
        toadSpawner.toads++;
        var bm1 = toad.GetComponent<BehaviourController>();
        var bm2 = toad.GetComponent<BehaviourController>().mate.GetComponent<BehaviourController>();
        toadsMating.Remove(toad);
        toadsMating.Remove(bm1.mate);
        bm1.mate = null;
        bm2.mate = null;
        bm1.waiter = false;
        bm2.waiter = false;
        bm1.GetComponent<FrogController>().GetComponent<Vitals>().foodLevel = 0;
        bm2.GetComponent<FrogController>().GetComponent<Vitals>().foodLevel = 0;
        bm1.Idle();
        bm2.Idle();
    }

    public void FrogDied(GameObject frog)
    {
        var bm1 = frog.GetComponent<BehaviourController>();
        BehaviourController bm2 = null;
        if (bm1.mate != null)
            bm2 = frog.GetComponent<BehaviourController>().mate.GetComponent<BehaviourController>();
        if (bm1.isFrog)
        {
            frogsReadyToMate.Remove(frog);
            frogsMating.Remove(frog);
            frogsMating.Remove(bm1.mate);
        }
        else
        {
            toadsReadyToMate.Remove(frog);
            toadsMating.Remove(frog);
            toadsMating.Remove(bm1.mate);
        }
        if (bm2 != null)
        {
            bm2.mate = null;
            bm2.waiter = false;
            bm2.Idle();
        }
    }
}
