﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Manage the main games UI
/// </summary>
public class UIController : MonoBehaviour
{
    public PathManager pathManager;
    public CameraController cameraController;
    public GameLoader gameLoader;
    public GameSettings gameSettings;

    [Header("MainUI")]
    public GameObject mainUI;
    public Toggle showNodes_Toggle;

    [Header("Frog Stats]")]
    public GameObject frogStats;
    public TMP_Text frogName;
    public TMP_Text frogFood;
    public TMP_Text frogFoodToMate;
    public TMP_Text frogDoing;
    private GameObject activeFrog;

    [Header("Car Stats")]
    public GameObject carStats;
    public TMP_Text carName;
    public TMP_Text carDoing;
    public RawImage dashCam;
    private GameObject activeCar;

    [Header("PauseMenu")]
    public bool isPaused;
    public GameObject pauseMenu;
    public GameObject exitConfirm;
    public GameObject saveConfirm;
    public GameObject loadConfirm;

    private void Start()
    {
        if (pathManager == null)
            pathManager = FindObjectOfType<PathManager>();
        if (cameraController == null)
            cameraController = FindObjectOfType<CameraController>();
        if (gameLoader == null)
            gameLoader = FindObjectOfType<GameLoader>();
        showNodes_Toggle.isOn = false;
        ToggleNodes();
    }

    private void Update()
    {
        if (frogStats.activeInHierarchy)
            ShowFrogStats(activeFrog);
        if (carStats.activeInHierarchy)
            ShowCarStats(activeCar);
        if (Input.GetKeyDown(KeyCode.Escape))
            TogglePause();
    }

    public void ToggleNodes()
    {
        pathManager.ShowNodesInGame();
    }

    public void ShowFrogStats(GameObject frog)
    {
        HideStats();
        if (frog != null)
        {
            activeFrog = frog;
            frogStats.SetActive(true);
            frogName.text = frog.name;
            frogFood.text = "Food Level: " + frog.GetComponent<Vitals>().foodLevel;
            frogFoodToMate.text = "Food to mate: " + frog.GetComponent<FrogController>().foodToMate;
            frogDoing.text = "Currently doing: " + frog.GetComponent<BehaviourController>().frogBehaviour;
        }
    }

    public void ShowCarStats(GameObject car)
    {
        HideStats();
        if (car != null)
        {
            activeCar = car;
            carStats.SetActive(true);
            carName.text = car.name;
            carDoing.text = "Currently doing: " + car.GetComponent<BehaviourController>().carBehaviour;
            dashCam.texture = car.GetComponent<CarController>().texture;
        }
    }

    public void HideStats()
    {
        frogStats.SetActive(false);
        carStats.SetActive(false);
    }

    public void TogglePause()
    {
        //Resume
        if (isPaused)
        {
            Time.timeScale = 1.0f;
            cameraController.EnableCursor(false);
            mainUI.SetActive(true);
            pauseMenu.SetActive(false);
        }
        else
        {
            Time.timeScale = 0.0f;
            cameraController.EnableCursor(true);
            mainUI.SetActive(false);
            pauseMenu.SetActive(true);
        }
        isPaused = !isPaused;
    }

    public void ResumeBtn()
    {
        TogglePause();
    }

    public void CheckSave()
    {
        saveConfirm.SetActive(true);
    }

    public void CheckSaveYes()
    {
        gameLoader.SaveGame();
        saveConfirm.SetActive(false);
    }

    public void CheckSaveNo()
    {
        saveConfirm.SetActive(false);
    }

    public void CheckLoad()
    {
        loadConfirm.SetActive(true);
    }

    public void CheckLoadYes()
    {
        TogglePause();
        gameSettings.startFromLoading = true;
        SceneManager.LoadScene(1);
    }

    public void CheckLoadNo()
    {
        loadConfirm.SetActive(false);
    }

    public void CheckExit()
    {
        exitConfirm.SetActive(true);
    }

    public void CheckExitYes()
    {
        SceneManager.LoadScene(0);
    }

    public void CheckExitNo()
    {
        exitConfirm.SetActive(false);
    }
}
