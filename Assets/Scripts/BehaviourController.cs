﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class BehaviourController : MonoBehaviour
{
    [Header("Shared")]
    PathAttendant pathAttendant;
    MovementMotor movementMotor;
    public bool isFrog;
    public bool isToad;
    public bool isCar;
    [Space]
    [Header("Frog Stuff")]
    public FrogBehaviours.FrogBehaviour frogBehaviour;
    public FoodManager foodManager;
    public GameObject currentFood;
    public MatingManger matingManger;
    public bool waitingForMate;
    public GameObject mate;
    public bool waiter;
    FrogController frogController;
    [Space]
    [Header("Car Stuff")]
    public CarBehaviours.CarBehaviour carBehaviour;
    public GameObject currentlyChasing;
    CarController carController;

    // Start is called before the first frame update
    void Start()
    {
        if (isToad || isFrog)
        {
            if (frogController == null)
                frogController = GetComponent<FrogController>();
            if (foodManager == null)
                foodManager = FindObjectOfType<FoodManager>();
            if (matingManger == null)
                matingManger = FindObjectOfType<MatingManger>();
        }
        else
        {
            if (carController == null)
                carController = GetComponent<CarController>();
        }
        if (movementMotor == null)
            movementMotor = GetComponent<MovementMotor>();
        if (pathAttendant == null)
            pathAttendant = FindObjectOfType<PathAttendant>();
        Idle();
    }

    // Update is called once per frame
    void Update()
    {
        if (isFrog || isToad)
        {
            if (IsIdling() && !movementMotor.navigating)
            {
                pathAttendant.AddToQueue(gameObject, movementMotor.RandomPoint());
            }
            else if (IsLookingForFood() && (currentFood == null || !currentFood.GetComponent<FoodController>().eatable))
            {
                if (foodManager.food.Count > 0)
                {
                    ////Get the closest food to this agent
                    //GameObject closestFood = null;
                    //for (int i = 0; i < foodManager.food.Count; i++)
                    //{
                    //    if (closestFood == null)
                    //    {
                    //        if (foodManager.food[i].GetComponent<FoodController>().eatable)
                    //            closestFood = foodManager.food[i];
                    //    }
                    //    else if (foodManager.food[i].GetComponent<FoodController>().eatable)
                    //    {
                    //        var closestDist = Vector3.Distance(transform.position, closestFood.transform.position);
                    //        var currDist = Vector3.Distance(transform.position, foodManager.food[i].transform.position);
                    //        if (currDist < closestDist)
                    //            closestFood = foodManager.food[i];
                    //    }
                    //}

                    //if (closestFood != null)
                    //{
                    //    currentFood = closestFood;
                    //    pathAttendant.AddToQueue(gameObject, currentFood.transform.position);
                    //}

                    currentFood = foodManager.food[Random.Range(0, foodManager.food.Count - 1)];
                    if (currentFood.GetComponent<FoodController>().eatable)
                        pathAttendant.AddToQueue(gameObject, currentFood.transform.position);
                    else
                    {
                        Idle();
                    }
                }
                else
                {
                    Idle();
                }
            }
            else if (IsLookingForMate())
            {
                if (mate != null && !waiter)
                    pathAttendant.AddToQueue(gameObject, mate.transform.position);
                else if (!waitingForMate && mate == null)
                {
                    if (isFrog) matingManger.frogsReadyToMate.Add(gameObject);
                    else if (isToad) matingManger.toadsReadyToMate.Add(gameObject);
                    waitingForMate = true;
                }
            }
            else if (!IsIdling() && currentFood == null)
            {
                Idle();
            }
        }
        else if (isCar)
        {
            if (IsIdling() && !movementMotor.navigating && !carController.overrideMovementMotor)
            {
                pathAttendant.AddToQueue(gameObject, movementMotor.RandomPoint());
            }
        }
    }

    public void Idle()
    {
        if (isFrog || isToad)
            frogBehaviour = FrogBehaviours.FrogBehaviour.IDLE;
        else
        {
            currentlyChasing = null;
            carBehaviour = CarBehaviours.CarBehaviour.IDLE;
        }
    }

    public bool IsIdling()
    {
        if (isFrog || isToad)
            return frogBehaviour == FrogBehaviours.FrogBehaviour.IDLE;
        else
            return carBehaviour == CarBehaviours.CarBehaviour.IDLE;
    }

    public void LookForFood()
    {
        frogBehaviour = FrogBehaviours.FrogBehaviour.LOOK_FOR_FOOD;
    }

    public bool IsLookingForFood()
    {
        if (isFrog || isToad)
            return frogBehaviour == FrogBehaviours.FrogBehaviour.LOOK_FOR_FOOD;
        else
            return false;
    }

    public void LookForMate()
    {
        frogBehaviour = FrogBehaviours.FrogBehaviour.LOOK_FOR_MATE;
    }

    public bool IsLookingForMate()
    {
        if (isFrog || isToad)
            return frogBehaviour == FrogBehaviours.FrogBehaviour.LOOK_FOR_MATE;
        else
            return false;
    }

    public void Chase(GameObject toChase)
    {
        carBehaviour = CarBehaviours.CarBehaviour.CHASE;
        currentlyChasing = toChase;
    }

    public bool IsChasing()
    {
        if (isCar)
            return carBehaviour == CarBehaviours.CarBehaviour.CHASE;
        else
            return false;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(BehaviourController))]
public class BehaviourControllerEditor : Editor
{
    override public void OnInspectorGUI()
    {
        var behaviourController = (BehaviourController)target;

        if (!behaviourController.isCar && !behaviourController.isToad)
            behaviourController.isFrog = GUILayout.Toggle(behaviourController.isFrog, "Is Frog");
        if (!behaviourController.isCar && !behaviourController.isFrog)
            behaviourController.isToad = GUILayout.Toggle(behaviourController.isToad, "Is Toad");
        if (!behaviourController.isFrog && !behaviourController.isToad)
            behaviourController.isCar = GUILayout.Toggle(behaviourController.isCar, "Is Car");
        else if (!behaviourController.isFrog && !behaviourController.isToad && !behaviourController.isCar)
        {
            behaviourController.isFrog = GUILayout.Toggle(behaviourController.isFrog, "Is Frog");
            behaviourController.isToad = GUILayout.Toggle(behaviourController.isToad, "Is Toad");
            behaviourController.isCar = GUILayout.Toggle(behaviourController.isCar, "Is Car");
        }

        if (behaviourController.isFrog || behaviourController.isToad)
        {
            behaviourController.frogBehaviour = (FrogBehaviours.FrogBehaviour)EditorGUILayout.EnumPopup("Frog Behaviour", behaviourController.frogBehaviour);
            behaviourController.foodManager = (FoodManager)EditorGUILayout.ObjectField("Food Manager", behaviourController.foodManager, typeof(FoodManager), true);
            behaviourController.currentFood = (GameObject)EditorGUILayout.ObjectField("Current Food", behaviourController.currentFood, typeof(GameObject), true);
            behaviourController.matingManger = (MatingManger)EditorGUILayout.ObjectField("Mating Manager", behaviourController.matingManger, typeof(MatingManger), true);
            behaviourController.waitingForMate = GUILayout.Toggle(behaviourController.waitingForMate, "Waiting For Mate");
            behaviourController.mate = (GameObject)EditorGUILayout.ObjectField("Mate", behaviourController.mate, typeof(GameObject), true);
            behaviourController.waiter = GUILayout.Toggle(behaviourController.waiter, "Waiter");
        }
        else if (behaviourController.isCar)
        {
            behaviourController.carBehaviour = (CarBehaviours.CarBehaviour)EditorGUILayout.EnumPopup("Car Behaviour", behaviourController.carBehaviour);
            behaviourController.currentlyChasing = (GameObject)EditorGUILayout.ObjectField("Currently Chasing", behaviourController.currentlyChasing, typeof(GameObject), true);
        }
    }
}
#endif