﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodManager : MonoBehaviour
{
    public bool debug;
    public GameObject foodPrefab;
    public GameObject pond;
    public PathManager pathManager;
    public GameSettings gameSettings;
    public Transform foodParent;
    public List<GameObject> food;

    public float scale = 20f;
    public float offsetX = 100f;
    public float offsetY = 100f;

    int numFood = 0;

    private void OnEnable()
    {
        EventManager.OnFrogsSpawned += SpawnFood;
    }

    private void OnDisable()
    {
        EventManager.OnFrogsSpawned -= SpawnFood;
    }

    private void Start()
    {
        food = new List<GameObject>();
        if (pathManager == null)
            pathManager = FindObjectOfType<PathManager>();
    }

    public void SpawnFood()
    {
        if (debug) Debug.Log("Spawning food");
        for (int i = 0; i < pathManager.pathDataNodes.Count; i++)
        {
            var node = pathManager.pathDataNodes[i];
            if (node.walkable)
            {
                if (gameSettings.foodDensity > Random.Range(0f, 1f) * Vector3.Distance(node.worldPos, pond.transform.position))
                {
                    var _food = Instantiate(foodPrefab, node.worldPos, foodPrefab.transform.rotation, foodParent);
                    var foodController = _food.GetComponent<FoodController>();
                    foodController.pond = pond;
                    food.Add(_food);
                    numFood++;
                }
            }
        }
        if (debug) Debug.Log("Done spawning food. Spawned " + numFood + " foods");
    }

    float CalculateChance(float x, float y)
    {
        float xCoord = (x + offsetX) / (pathManager.width * scale);
        float yCoord = (y + offsetY) / (pathManager.height * scale);
        return Mathf.PerlinNoise(xCoord, yCoord);
    }

    public void RemoveFood(GameObject toRemove)
    {
        toRemove.GetComponent<FoodController>().GetEaten();
    }
}
