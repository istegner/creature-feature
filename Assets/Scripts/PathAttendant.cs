﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathAttendant : MonoBehaviour
{
    public int maxAbleToPathFindPerFrame;
    //public int framesToWaitBeforeAllowingToPathFind;
    public int framesBetweenRides;

    private Dictionary<GameObject, Vector3> agents = new Dictionary<GameObject, Vector3>();
    [SerializeField] private List<GameObject> waitingToPathFind = new List<GameObject>();
    private int currentFrame;
    private int prevFrame;
    private int pathFoundThisFrame;

    private void Update()
    {
        pathFoundThisFrame = 0;
        currentFrame = Time.frameCount;
        if (Time.frameCount - prevFrame >= framesBetweenRides)
        {
            prevFrame = currentFrame;
            for (int i = 0; i < waitingToPathFind.Count; i++)
            {
                if (pathFoundThisFrame >= maxAbleToPathFindPerFrame)
                    break;
                if (waitingToPathFind[0] != null)
                {
                    AllowToPathFind(waitingToPathFind[0]);
                    pathFoundThisFrame++;
                }
                else
                {
                    waitingToPathFind.RemoveAt(0);
                    i--;
                }
            }
        }
    }

    public void AddToQueue(GameObject toAdd, Vector3 destination)
    {
        if (!waitingToPathFind.Contains(toAdd))
        {
            waitingToPathFind.Add(toAdd);
            if (agents.ContainsKey(toAdd))
                agents[toAdd] = destination;
            else
                agents.Add(toAdd, destination);
        }
    }

    public void RemoveFromQueue(GameObject toRemove)
    {
        if (waitingToPathFind.Contains(toRemove))
            waitingToPathFind.Remove(toRemove);
    }

    private void AllowToPathFind(GameObject agent)
    {
        MovementMotor mm = agent.GetComponent<MovementMotor>();
        if (mm != null) mm.GetPath(agents[agent]);
        else Debug.LogError(agent.name + "is waiting for a path and does not have a Movement Motor", agent);
        waitingToPathFind.Remove(agent);
    }
}
