﻿[System.Serializable]
public class FrogBehaviours
{
    public FrogBehaviour frogBehaviour;

    public enum FrogBehaviour
    {
        IDLE,
        LOOK_FOR_FOOD,
        LOOK_FOR_MATE,
        RUN_AWAY
    }
}
